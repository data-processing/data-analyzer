package com.mycompany.dp.analyzer.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.dp.analyzer.AnalyzerApplication;
import com.mycompany.dp.analyzer.jms.EventAnalyzerPojo;
import com.mycompany.dp.analyzer.model.UserData;
import com.mycompany.dp.analyzer.repository.UserRepository;
import com.mycompany.dp.analyzer.service.AggregationService;
import java.time.LocalDateTime;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles( {"test"})
@SpringBootTest(classes = {AnalyzerApplication.class})
public class AggregationServiceTest {

  @Autowired
  private AggregationService aggregationService;

  @Autowired
  private UserRepository userRepository;

  @Test
  public void should_handleEventData() {
    final LocalDateTime now = LocalDateTime.now();

    final EventAnalyzerPojo event1 = getEvent(UUID.randomUUID(), UUID.randomUUID(), now);
    aggregationService.handleEventData(event1);

    // do second click for the same user during same day
    final EventAnalyzerPojo event2 = getEvent(event1.getUserId(), event1.getScreenId(), now);
    aggregationService.handleEventData(event2);

    // do third click for the different day
    final EventAnalyzerPojo event3 = getEvent(UUID.randomUUID(), UUID.randomUUID(), now);
    aggregationService.handleEventData(event3);

    final UserData userData = userRepository
        .findByUserIdAndScreenIdAndCreatedDate(event1.getUserId(), event1.getScreenId(), now.toLocalDate())
        .blockFirst();
    assertThat(userData.getRequestIds()).hasSize(2)
        .containsExactlyInAnyOrder(event1.getRequestId(), event2.getRequestId());
    assertThat(userData.getEmail()).isEqualTo(event1.getEmail());
    assertThat(userData.getScreen()).isEqualTo(event1.getScreen());
  }

  private EventAnalyzerPojo getEvent(UUID userId, UUID screenId, LocalDateTime clickTime) {
    return EventAnalyzerPojo.builder()
        .requestId(UUID.randomUUID().toString())
        .userId(userId)
        .email(userId.toString() + "@test.com")
        .name(userId.toString())
        .screenId(screenId)
        .screen(screenId.toString())
        .clickTime(clickTime).build();
  }

}
