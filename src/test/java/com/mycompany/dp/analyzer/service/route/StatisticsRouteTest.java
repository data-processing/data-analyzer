package com.mycompany.dp.analyzer.service.route;

import static com.mycompany.dp.analyzer.route.StatisticsRoute.USER_STATS;
import static com.mycompany.dp.analyzer.route.StatisticsRoute.USER_STATS_PER_SCREEN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.documentationConfiguration;

import com.mycompany.dp.analyzer.AnalyzerApplication;
import com.mycompany.dp.analyzer.dto.UserStatsPerScreenResponseDto;
import com.mycompany.dp.analyzer.dto.UserStatsResponseDto;
import com.mycompany.dp.analyzer.jms.EventAnalyzerPojo;
import com.mycompany.dp.analyzer.route.StatisticsRoute;
import com.mycompany.dp.analyzer.service.AggregationService;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

@RunWith(SpringRunner.class)
@ActiveProfiles( {"test"})
@SpringBootTest(classes = {AnalyzerApplication.class})
public class StatisticsRouteTest {

  @Rule
  public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

  @Autowired
  private ApplicationContext context;
  private WebTestClient webTestClient;

  @Autowired
  private AggregationService aggregationService;


  @Before
  public void setUp() {
    this.webTestClient = WebTestClient.bindToApplicationContext(this.context)
        .configureClient()
        .filter(documentationConfiguration(this.restDocumentation))
        .build();
  }

  @Test
  public void should_getStatsForUserPerScreen() {
    final LocalDateTime now = LocalDateTime.of(2021, 1, 1, 1, 1);

    final EventAnalyzerPojo eventPojo = getEvent("jhon@smith.com", "Options", now);
    aggregationService.handleEventData(eventPojo);

    // do second click for the same user during same day
    eventPojo.setClickTime(now.plusSeconds(10));
    eventPojo.setRequestId(UUID.randomUUID().toString());
    aggregationService.handleEventData(eventPojo);

    // do second click for the same user during same day
    eventPojo.setClickTime(now.plusSeconds(30));
    eventPojo.setRequestId(UUID.randomUUID().toString());
    aggregationService.handleEventData(eventPojo);

    webTestClient
        .get()
        .uri(StatisticsRoute.USER_STATS_PER_SCREEN_PATH,
            eventPojo.getEmail(), eventPojo.getScreen(), now.format(DateTimeFormatter.ISO_LOCAL_DATE))
        .exchange()
        .expectStatus().isOk()
        .expectBody(UserStatsPerScreenResponseDto.class)
        .value(userStats -> {
              assertThat(userStats).isNotNull();
              assertThat(userStats.getEmail()).isEqualTo(eventPojo.getEmail());
              assertThat(userStats.getScreen()).isEqualTo(eventPojo.getScreen());
              assertThat(userStats.getAvgClickTimeInSeconds()).isEqualTo(15.0);
            }
        ).consumeWith(document(USER_STATS_PER_SCREEN, pathParameters(
        parameterWithName("email").description("Email address of the user"),
        parameterWithName("screen").description("The screen for the statistics"),
        parameterWithName("forDate").description("The date of the statistics")
    )));


  }

  @Test
  public void should_getStatsForUser() {
    final LocalDateTime now = LocalDateTime.of(2021, 1, 1, 1, 1);

    final EventAnalyzerPojo eventPojo = getEvent("happy@day.com", "Main", now);
    aggregationService.handleEventData(eventPojo);

    // do second click for the same user during same day
    eventPojo.setClickTime(now.plusSeconds(10));
    eventPojo.setRequestId(UUID.randomUUID().toString());
    aggregationService.handleEventData(eventPojo);

    // do second click for the same user during same day
    eventPojo.setClickTime(now.plusSeconds(20));
    eventPojo.setRequestId(UUID.randomUUID().toString());
    eventPojo.setScreen("About");
    eventPojo.setScreenId(UUID.randomUUID());
    aggregationService.handleEventData(eventPojo);

    webTestClient
        .get()
        .uri(StatisticsRoute.USER_STATS_PATH, eventPojo.getEmail(), now.format(DateTimeFormatter.ISO_LOCAL_DATE))
        .exchange()
        .expectStatus().isOk()
        .expectBody(UserStatsResponseDto.class)
        .value(userStats -> {
              assertThat(userStats).isNotNull();
              assertThat(userStats.getEmail()).isEqualTo(eventPojo.getEmail());
              assertThat(userStats.getAvgClickCount()).isEqualTo(1.5);
            }
        ).consumeWith(document(USER_STATS, pathParameters(
        parameterWithName("email").description("Email address of the user"),
        parameterWithName("forDate").description("The date of the statistics"))));

  }

  private EventAnalyzerPojo getEvent(String email, String screen, LocalDateTime clickTime) {
    return EventAnalyzerPojo.builder()
        .requestId(UUID.randomUUID().toString())
        .userId(UUID.randomUUID())
        .email(email)
        .name(email.split("@")[0])
        .screenId(UUID.randomUUID())
        .screen(screen)
        .clickTime(clickTime).build();
  }

}
