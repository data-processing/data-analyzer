package com.mycompany.dp.analyzer.utils;

import lombok.experimental.UtilityClass;

/**
 * Here are defined shared constants.
 */
@UtilityClass
public class AnalyzerConstants {

  public static final String EVENT_REQUEST_ID = "requestId";
}
