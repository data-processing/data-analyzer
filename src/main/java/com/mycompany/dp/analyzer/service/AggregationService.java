package com.mycompany.dp.analyzer.service;

import com.mycompany.dp.analyzer.dto.UserStatsPerScreenRequestDto;
import com.mycompany.dp.analyzer.dto.UserStatsPerScreenResponseDto;
import com.mycompany.dp.analyzer.dto.UserStatsRequestDto;
import com.mycompany.dp.analyzer.dto.UserStatsResponseDto;
import com.mycompany.dp.analyzer.jms.EventAnalyzerPojo;
import reactor.core.publisher.Mono;

/**
 * Service interface for statistics data.
 */
public interface AggregationService {

  void handleEventData(EventAnalyzerPojo event);

  Mono<UserStatsPerScreenResponseDto> getUserStatsForScreenAndDate(
      UserStatsPerScreenRequestDto request);

  Mono<UserStatsResponseDto> getUserStatsForDate(UserStatsRequestDto request);

}
