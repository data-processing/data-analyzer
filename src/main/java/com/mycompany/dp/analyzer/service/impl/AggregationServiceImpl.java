package com.mycompany.dp.analyzer.service.impl;

import com.mycompany.dp.analyzer.dto.UserStatsPerScreenRequestDto;
import com.mycompany.dp.analyzer.dto.UserStatsPerScreenResponseDto;
import com.mycompany.dp.analyzer.dto.UserStatsRequestDto;
import com.mycompany.dp.analyzer.dto.UserStatsResponseDto;
import com.mycompany.dp.analyzer.jms.EventAnalyzerPojo;
import com.mycompany.dp.analyzer.model.UserData;
import com.mycompany.dp.analyzer.repository.UserRepository;
import com.mycompany.dp.analyzer.service.AggregationService;
import io.micrometer.core.instrument.MeterRegistry;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

/**
 * Aggregates and provide statistics for user.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AggregationServiceImpl implements AggregationService {

  private final UserRepository userRepository;
  private final MeterRegistry meterRegistry;

  @Override
  @Transactional
  public void handleEventData(EventAnalyzerPojo event) {
    final LocalDate eventDate = event.getClickTime().toLocalDate();
    final UserData userData = userRepository.findByUserIdAndScreenIdAndCreatedDate(
        event.getUserId(), event.getScreenId(), eventDate)
        .defaultIfEmpty(UserData.builder()
            .requestIds(new HashSet<>())
            .createdDate(eventDate)
            .email(event.getEmail())
            .userId(event.getUserId())
            .screenId(event.getScreenId())
            .clicksTime(new HashSet<>())
            .screen(event.getScreen()).build())
        .blockFirst();

    userData.getRequestIds().add(event.getRequestId());
    userData.getClicksTime().add(event.getClickTime());

    userRepository.save(userData)
        .subscribe(e -> log.info("Stat saved {}", e));
  }

  @Override
  public Mono<UserStatsPerScreenResponseDto> getUserStatsForScreenAndDate(
      UserStatsPerScreenRequestDto request) {
    return userRepository.findByEmailAndScreenAndCreatedDate(
        request.getEmail(), request.getScreen(), request.getForDate())
        .next()
        .flatMap(e -> getCalculatedResponse(e))
        .doOnNext(e -> meterRegistry.counter("userStatsScreen.retrieved", "email",
            e.getEmail(), "screen", e.getScreen()).increment());
  }

  private Mono<UserStatsPerScreenResponseDto> getCalculatedResponse(UserData userData) {
    final List<LocalDateTime> clicksTime = userData.getClicksTime()
        .stream()
        .sorted()
        .collect(Collectors.toList());
    LocalDateTime before = clicksTime.get(0);
    final List<Long> periods = new ArrayList<>();
    for (int i = 1; i < clicksTime.size(); i++) {

      periods.add(ChronoUnit.SECONDS.between(before, clicksTime.get(i)));
      before = clicksTime.get(i);
    }
    final double avgClickTimeInSeconds = periods.stream()
        .mapToLong(Long::longValue)
        .average()
        .orElse(0);

    return Mono.just(UserStatsPerScreenResponseDto.builder()
        .screen(userData.getScreen())
        .email(userData.getEmail())
        .avgClickTimeInSeconds(avgClickTimeInSeconds)
        .build());
  }


  @Override
  public Mono<UserStatsResponseDto> getUserStatsForDate(UserStatsRequestDto request) {
    return userRepository.findByEmailAndCreatedDate(
        request.getEmail(), request.getForDate())
        .flatMap(e -> Mono.just(e.getRequestIds().size()))
        .collect(Collectors.averagingInt(Integer::intValue))
        .flatMap(avg -> Mono.just(UserStatsResponseDto.builder()
            .email(request.getEmail())
            .avgClickCount(avg)
            .build()))
        .doOnNext(e -> meterRegistry.counter("userStats.retrieved", "email",
            e.getEmail()).increment());

  }

}
