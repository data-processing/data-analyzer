package com.mycompany.dp.analyzer.jms;

import static com.mycompany.dp.analyzer.utils.AnalyzerConstants.EVENT_REQUEST_ID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.dp.analyzer.service.AggregationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Slf4j
@Component
@Profile("!test")
public class JmsAnalyzerManager {

  private final AggregationService aggregationService;
  private final ObjectMapper objectMapper;

  @JmsListener(destination = "${analyzer.queueName:analyzer}",
      containerFactory = "jmsListenerContainerFactory")
  public void processEvent(String eventData) throws JsonProcessingException {
    final EventAnalyzerPojo event = objectMapper.readValue(eventData, EventAnalyzerPojo.class);
    MDC.put(EVENT_REQUEST_ID, event.getRequestId());
    log.info("Event aggregation started");

    aggregationService.handleEventData(event);

    log.info("Event aggregation ended");
  }
}
