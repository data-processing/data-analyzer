package com.mycompany.dp.analyzer.handler;

import static org.springframework.http.MediaType.APPLICATION_JSON;

import com.mycompany.dp.analyzer.dto.UserStatsPerScreenRequestDto;
import com.mycompany.dp.analyzer.dto.UserStatsRequestDto;
import com.mycompany.dp.analyzer.exception.ExceptionManager;
import com.mycompany.dp.analyzer.service.AggregationService;
import com.mycompany.dp.analyzer.validator.RequestValidator;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Slf4j
@Component
@AllArgsConstructor
public class StatisticsHandler {

  private final AggregationService aggregationService;
  private final RequestValidator requestValidator;

  public Mono<ServerResponse> getStatsForUserPerScreen(ServerRequest serverRequest) {
    log.info("Request for user stats per screen {}", serverRequest.pathVariables());

    return getUserStatsPerScreenRequestDto(serverRequest)
        .flatMap(aggregationService::getUserStatsForScreenAndDate)
        .flatMap(response -> ServerResponse.ok()
            .contentType(APPLICATION_JSON)
            .bodyValue(response))
        .switchIfEmpty(ServerResponse.notFound().build())
        .onErrorResume(ExceptionManager::toErrorResponse);
  }

  private Mono<UserStatsPerScreenRequestDto> getUserStatsPerScreenRequestDto(ServerRequest serverRequest) {
    final UserStatsPerScreenRequestDto request = UserStatsPerScreenRequestDto.builder()
        .screen(serverRequest.pathVariable("screen"))
        .email(serverRequest.pathVariable("email"))
        .forDate(LocalDate.parse(serverRequest.pathVariable("forDate"), DateTimeFormatter.ISO_LOCAL_DATE))
        .build();

    requestValidator.validate(request);

    return Mono.just(request);
  }

  public Mono<ServerResponse> getStatsForUser(ServerRequest serverRequest) {
    log.info("Request for user stats {}", serverRequest.pathVariables());

    return getUserStatsRequestDto(serverRequest)
        .flatMap(request -> aggregationService.getUserStatsForDate(request))
        .flatMap(response -> ServerResponse.ok()
            .contentType(APPLICATION_JSON)
            .bodyValue(response))
        .switchIfEmpty(ServerResponse.notFound().build())
        .onErrorResume(ExceptionManager::toErrorResponse);
  }

  private Mono<UserStatsRequestDto> getUserStatsRequestDto(ServerRequest serverRequest) {
    final UserStatsRequestDto request = UserStatsRequestDto.builder()
        .email(serverRequest.pathVariable("email"))
        .forDate(LocalDate.parse(serverRequest.pathVariable("forDate"), DateTimeFormatter.ISO_LOCAL_DATE))
        .build();

    requestValidator.validate(request);

    return Mono.just(request);
  }
}
