package com.mycompany.dp.analyzer.route;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

import com.mycompany.dp.analyzer.dto.ErrorResponseDto;
import com.mycompany.dp.analyzer.dto.UserStatsPerScreenResponseDto;
import com.mycompany.dp.analyzer.handler.StatisticsHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springdoc.core.annotations.RouterOperation;
import org.springdoc.core.annotations.RouterOperations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class StatisticsRoute {

  public static final String BASE_PATH = "/api/v1/statistics";
  public static final String USER_STATS_PER_SCREEN_PATH = BASE_PATH +
      "/user/{email}/screen/{screen}/{forDate}";
  public static final String USER_STATS_PATH = BASE_PATH + "/user/{email}/{forDate}";
  public static final String USER_STATS_PER_SCREEN = "user-stats-per-screen";
  public static final String USER_STATS = "user-stats";
  private static final String TAG = "STATISTICS";

  @RouterOperations( {
      @RouterOperation(path = USER_STATS_PER_SCREEN_PATH, method = RequestMethod.GET,
          operation = @Operation(operationId = USER_STATS_PER_SCREEN, tags = TAG,
              summary = "Get statistics for user per screen and day",
              parameters = {
                  @Parameter(in = ParameterIn.PATH, name = "email", description = "Email of user",
                      example = "jhon@smith.com"),
                  @Parameter(in = ParameterIn.PATH, name = "screen", description = "Clicked screen",
                      example = "Options"),
                  @Parameter(in = ParameterIn.PATH, name = "forDate", description = "Statistics date",
                      example = "2021-04-25"),
              },
              responses = {
                  @ApiResponse(responseCode = "200",
                      content = @Content(schema = @Schema(implementation = UserStatsPerScreenResponseDto.class))),
                  @ApiResponse(responseCode = "412", description = "Validation failed on field(s)",
                      content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
                  @ApiResponse(responseCode = "404", description = "Statistics not found",
                      content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
              })),
      @RouterOperation(path = USER_STATS_PATH, method = RequestMethod.GET,
          operation = @Operation(operationId = USER_STATS, tags = TAG, summary = "Get statistics for user per day",
              parameters = {
                  @Parameter(in = ParameterIn.PATH, name = "email", description = "Email of user",
                      example = "jhon@smith.com"),
                  @Parameter(in = ParameterIn.PATH, name = "forDate", description = "Statistics date",
                      example = "2021-04-25"),
              },
              responses = {
                  @ApiResponse(responseCode = "200",
                      content = @Content(schema = @Schema(implementation = UserStatsPerScreenResponseDto.class))),
                  @ApiResponse(responseCode = "412", description = "Validation failed on field(s)",
                      content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
                  @ApiResponse(responseCode = "404", description = "Statistics not found",
                      content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
              }))
  })

  @Bean
  public RouterFunction<ServerResponse> userRouter(StatisticsHandler handler) {
    return RouterFunctions
        .route(GET(USER_STATS_PER_SCREEN_PATH).and(accept(APPLICATION_JSON)), handler::getStatsForUserPerScreen)
        .andRoute(GET(USER_STATS_PATH).and(accept(APPLICATION_JSON)), handler::getStatsForUser);
  }
}
