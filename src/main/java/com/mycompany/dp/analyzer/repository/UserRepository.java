package com.mycompany.dp.analyzer.repository;

import com.mycompany.dp.analyzer.model.UserData;
import java.time.LocalDate;
import java.util.UUID;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface UserRepository
    extends ReactiveMongoRepository<UserData, String> {

  Flux<UserData> findByUserIdAndScreenIdAndCreatedDate(UUID userId, UUID screenId, LocalDate createdDate);

  Flux<UserData> findByEmailAndScreenAndCreatedDate(String email, String screen, LocalDate createdDate);

  Flux<UserData> findByEmailAndCreatedDate(String email, LocalDate createdDate);
}
