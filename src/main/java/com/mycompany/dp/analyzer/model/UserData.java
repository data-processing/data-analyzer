package com.mycompany.dp.analyzer.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@CompoundIndex(name = "uk_user_screen_date",
    def = "{'user_id' : 1, 'screen_id': 1, 'created_date' : 1}")
public class UserData implements Serializable {

  public static final long serialVersionUID = 1L;

  @Id
  private String id;

  @Indexed
  @NotNull
  @Field(value = "email")
  private String email;

  @Indexed
  @NotNull
  @Field(value = "user_id")
  private UUID userId;

  @Indexed
  @NotNull
  @Field(value = "screen")
  private String screen;

  @Indexed
  @NotNull
  @Field(value = "screen_id")
  private UUID screenId;

  @Indexed
  @NotNull
  @Field(value = "created_date")
  private LocalDate createdDate;

  @Field(value = "request_ids")
  @NotNull
  private Set<String> requestIds;

  @Field(value = "clicks_time")
  @NotNull
  private Set<LocalDateTime> clicksTime;
}
