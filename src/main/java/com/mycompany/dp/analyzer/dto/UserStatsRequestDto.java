package com.mycompany.dp.analyzer.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDate;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserStatsRequestDto {

  @NotNull
  @Schema(description = "Email address of the user.",
      example = "jhon@smith.com", required = true)
  private String email;

  @Schema(description = "Statistics date",
      example = "2021-04-25", required = true)
  @NotNull
  private LocalDate forDate;

}
