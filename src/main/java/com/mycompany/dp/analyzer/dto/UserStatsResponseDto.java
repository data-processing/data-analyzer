package com.mycompany.dp.analyzer.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserStatsResponseDto {

  @Schema(description = "Email address of the user.",
      example = "jhon@smith.com")
  private String email;

  @Schema(description = "Average clicks count users perform during a given time period.",
      example = "45.5")
  private Double avgClickCount;

}
