package com.mycompany.dp.analyzer.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserStatsPerScreenResponseDto {

  @Schema(description = "Email address of the user.",
      example = "jhon@smith.com")
  private String email;

  @Schema(description = "Screen of the user.",
      example = "Options")
  private String screen;

  @Schema(description = "Average time users spent on the appropriate screen.",
      example = "Options")
  private Double avgClickTimeInSeconds;

}
