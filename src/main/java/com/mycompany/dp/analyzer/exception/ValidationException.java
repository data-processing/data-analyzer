package com.mycompany.dp.analyzer.exception;

import com.mycompany.dp.analyzer.dto.ValidationErrorResponseDto;
import java.util.List;
import lombok.Getter;

@Getter
public class ValidationException extends BaseException {

  private final transient List<ValidationErrorResponseDto> errorArgs;


  public ValidationException(ErrorEnum errorEnum, List<ValidationErrorResponseDto> errorArgs) {
    super(errorEnum);
    this.errorArgs = errorArgs;
  }

}
