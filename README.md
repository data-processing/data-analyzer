# Data Analyzer

Data Analyzer: works with aggregated data to provide required statistics.

For the further information please refer to the platform [official documentation](https://gitlab.com/data-processing/data-processing-meta/-/blob/master/README.md)  
 
